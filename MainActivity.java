package com.Muse;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {
	
	private WebView musewebview;
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.main);
		initialize();
		initializeLogic();
	}
	
	private void initializeLogic() {
		musewebview = (WebView) findViewById(R.id.musewebview);
		musewebview.getSettings.setJavaScriptEnabled(true);
		musewebview.getSettings.setSupportZoom(true);
		
		musewebview.setWebViewClient(new WebViewClient() {
			public void onPageStarted(WebView _param1, String _param2, Bitmap _param3) {
				final String _url = _param2;
				musewebview.getSettings().SetCacheMode(WebSettings.LOAD_NO_CACHE);
				super.onPageStarted(_param1,_param2,_param3);
			}
			
			public void onPageFinished(WebView _param1, String _param2) {
				final String _url = _param2;
				super.onPageFinished(_param1,_param2);
			}
		});
	}
	
	private void initializeLogic() {
		musewebview.loadUrl("https://gitlab.com/GeneralKenobee/Muse-WebView/");
	}
	
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode,_resultCode,_data);
		
		switch (_requestCode) {
			default:
			break;
		}
	}
	
	public void onDestroy() {
		super.onDestroy();
		musewebview.clearCache(true);
		finish();
	}
	
	public void onBackpressed() {
		//todo: fix this. in this case, the app will close onBackpressed() but should got back -1 on history.
	}
	
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(),_s, Toast.LENGTH_SHORT).show();
	}
	
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindows(_location);
		return _location[0];
	}
	
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindows(_location);
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _ildx = 0; _ildx < _arr.size(); _ildx++) {
			if (_arr.valueAt(_ildx)) {
				_result.add((double)_arr.keyAt(_ildx));
			}
		}
		
		return _result;
	}
	
	public float getDip(int _input) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	public int getDisplayWidthPixels() {
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	public int getDisplayHeightPixels() {
		return getResources().getDisplayMetrics().heightPixels;
	}
}